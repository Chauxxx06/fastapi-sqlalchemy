from fastapi import FastAPI
from config.database import engine, base
from middleware.errorHandler import ErrorHandler
from routers.movie import movieRoute # incluye los archivos de router
from routers.users import userRoute

app = FastAPI()
app.title = "Mi aplicación con  FastAPI"
app.version = "0.0.1"
app.add_middleware(ErrorHandler) # se agrega el middleware al app de fastapi
app.include_router(movieRoute) # la agreaga a la app del router
app.include_router(userRoute) # la agreaga a la app del router

base.metadata.create_all(bind=engine)


