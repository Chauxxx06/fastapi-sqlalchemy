from starlette.middleware.base import BaseHTTPMiddleware # libreria de manejo de middleware
from fastapi import FastAPI, Request, Response
from fastapi.responses import JSONResponse

# clase para manejo de errores
class ErrorHandler(BaseHTTPMiddleware):
    # El constructor se le debe indicar que el framework es fastapi
    def __init__(self, app: FastAPI) -> None:
        super().__init__(app)

    async def dispatch(self, request: Request, call_next) -> Response or JSONResponse:
        try:
            return await call_next (request)
        except Exception as e:
            return JSONResponse(status_code=500, content={'error': str(e)})
