from models.Movie import Movie as MovieModel
from schemas.movie import Movie as MovieSchema

class MovieService():

    def __init__(self, db) -> None:
        self.db = db

    def get_movie(self):
        result = self.db.query(MovieModel).all()
        return result

    def get_unique_movie(self, id):
        result = self.db.query(MovieModel).filter(MovieModel.id == id).first()
        return result
    
    def get_movies_by_category(self, category):
        result = self.db.query(MovieModel).filter(MovieModel.category == category).all()
        return result

    def create_movie(self, movie: MovieSchema):
        newMovie = MovieModel(**movie.dict())
        self.db.add(newMovie)
        self.db.commit()
        return

    def update_movie(self, id: int, data: MovieSchema):
        movie = self.db.query(MovieModel).filter(MovieModel.id == id).first()
        movie.title = data.title
        movie.overview = data.overview
        movie.year = data.year
        movie.rating = data.rating
        movie.category = data.category
        self.db.commit()
        return

    def delete_movie(self, id: int):
        self.db.delete(id)
        self.db.commit()