import os
from sqlalchemy import create_engine
from sqlalchemy.orm.session import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

sqliteDatabase =  "../database.sqlite"
baseDir = os.path.dirname(os.path.realpath(__file__)) # Lee directorio actual 
print(baseDir)

databaseUrl = f"sqlite:///{os.path.join(baseDir,sqliteDatabase)}"
print(databaseUrl)

engine = create_engine(databaseUrl, echo=True) # de la ruta de la base de datos conecte el motor para procesar y manipular
session = sessionmaker(bind = engine) # se crea una sesion del motor
base = declarative_base() # para manipular las tablas