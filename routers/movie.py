from fastapi import APIRouter
from models.Movie import Movie as MovieModel
from fastapi import Path, Query
from fastapi.responses import JSONResponse
from schemas.movie import Movie
from typing import List
from config.database import session
from fastapi.encoders import jsonable_encoder
from service.movie import MovieService

movieRoute = APIRouter()


@movieRoute.get('/movies', tags=['movies'], response_model=List[Movie], status_code=200)
def get_movies() -> List[Movie]:
    db = session()
    result = MovieService(db).get_movie()
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

@movieRoute.get('/movies/{id}', tags=['movies'], response_model=Movie)
def get_movie(id: int = Path(ge=1, le=2000)) -> Movie:
    db = session()
    result = MovieService(db).get_unique_movie(id)
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

@movieRoute.get('/movies/', tags=['movies'], response_model=List[Movie])
def get_movies_by_category(category: str = Query(min_length=5, max_length=15)) -> List[Movie]:
    db = session()
    result = MovieService(db).get_movies_by_category(category)
    return JSONResponse(content=result)

@movieRoute.post('/movies', tags=['movies'], response_model=dict, status_code=201)
def create_movie(movie: Movie) -> dict:
    db = session()
    MovieService(db).create_movie(movie)
    return JSONResponse(status_code=201, content={"message": "Se ha registrado la película"})

@movieRoute.put('/movies/{id}', tags=['movies'], response_model=dict, status_code=200)
def update_movie(id: int, movie: Movie)-> dict:
    db = session()
    result = MovieService(db).get_unique_movie(id)
    if not result:
        return JSONResponse(status_code=404, content={'message': "Not found"})
    MovieService(db).update_movie(id, movie)
    return JSONResponse(status_code=200, content={"message": "Se ha modificado la película"})

@movieRoute.delete('/movies/{id}', tags=['movies'], response_model=dict, status_code=200)
def delete_movie(id: int)-> dict:
    db = session()
    result = MovieService(db).get_unique_movie(id)
    if not result:
        return JSONResponse(status_code=404, content={'message': "Not found"})
    MovieService(db).delete_movie(result)
    return JSONResponse(status_code=200, content={"message": "Se ha eliminado la película"})