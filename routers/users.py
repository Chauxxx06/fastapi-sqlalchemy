from fastapi import APIRouter
from fastapi.responses import JSONResponse
from middleware.jwt_bearer import JWTBearer
from schemas.user import User

userRoute = APIRouter()

@userRoute.post('/login', tags=['auth'])
def login(user: User):
    if user.email == "admin@gmail.com" and user.password == "admin":
        token: str = JWTBearer.create_token(user.dict())
        return JSONResponse(status_code=200, content=token)
